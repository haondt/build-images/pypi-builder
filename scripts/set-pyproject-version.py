import argparse
import toml

parser = argparse.ArgumentParser(prog='python-builder')
parser.add_argument('-f', '--file' , default='pyproject.toml', help='pyproject toml file')
parser.add_argument('version', help='version string')

args = parser.parse_args()
file = args.file
version = args.version

data = toml.load(file)
data['project']['version'] = version
with open(file, 'w') as f:
    toml.dump(data, f)

