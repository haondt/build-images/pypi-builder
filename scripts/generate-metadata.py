import argparse

parser = argparse.ArgumentParser(prog='python-builder')
parser.add_argument('-v', '--version' , help='pyproject toml file', required=True)

args = parser.parse_args()

output = []
output.append(f'__version__ = "{args.version}"')
print('\n'.join(output))
